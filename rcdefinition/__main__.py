#!/usr/bin/env python3
"""Main entry point, command-line interface for rcdefinition."""

import click

from rcdefinition.trigger_variables import TriggerVariables

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# Allow passing click context.
# pylint: disable=unused-argument

@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx):
    """Click cli entry point."""


@click.group()
@click.pass_context
def triggervars(ctx):
    """Commands to work with trigger variables."""


@triggervars.command(name='print')
@click.pass_context
def print_command(ctx):
    """Print all trigger variables."""
    TriggerVariables().print_var_names()


cli.add_command(triggervars)


def main():
    """Do everything; main entrypoint."""
    # pylint: disable=no-value-for-parameter
    cli()


if __name__ == '__main__':
    main()
